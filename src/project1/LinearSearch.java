package project1;

public class LinearSearch {
	static int linearSearch (int[] A, int[] L, int count) {
		
		for (int i = 0; i < A.length; i ++) {
			int max = 0; 
			for (int j = 0; j < A.length; j++) {
				count ++;
				if (A[j] > A[max]) {
					max = j;
				}
			}
			L[i] = A[max];
			A[max] = Integer.MIN_VALUE; //Do not want to count same number on future iterations
		}
		return count;
	}

}
