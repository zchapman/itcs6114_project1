package project1;
import java.util.*;


public class Project1 {
	static void Swap(int[] A, int x, int y) {
		A[x] = A[x] + A[y];
		A[y] = A[x] - A[y];
		A[x] = A[x] - A[y];
	}

	public static void main(String[] args) {
		
		int linCount = 0;
		int mergeCount = 0;
		int heapCount = 0;
		int quickCount = 0;
		int[] c = new int [1];
		int size = 10;
		int element = 5;
		//Input arrays (all copies)
		int[] Array1 = new int[size]; 
		int[] Array2 = new int[size];
		int[] Array3 = new int[size];
		int[] Array4 = new int[size];
		//Output arrays (theoretically all copies)
		int[] L = new int[size];
		int[] M = new int[element];
		int[] H = new int[element];
		int[] Q = new int[element];
		
		Random rand = new Random();
		
		//Initialize input array
		System.out.println("Array to be sorted by all algorithms: ");
		for (int i = 0; i < size; i ++) {
			Array1[i] = rand.nextInt(50000) + 1;
			System.out.print(Array1[i] + ", ");
		}
		System.out.println();
		
		//Make copies
		System.arraycopy(Array1, 0, Array2, 0, size);
		System.arraycopy(Array1, 0, Array3, 0, size);
		System.arraycopy(Array1, 0, Array4, 0, size);
		
		//Run linear search on first array
		linCount = LinearSearch.linearSearch(Array1, L, linCount);
		
		//Run mergesort on second array
		mergeCount = MergeSort.mergeSort(Array2, 0, size-1, mergeCount);
		for (int i = 0; i < element; i++) {
			M[i] = Array2[size - i - 1];
		}
		//Run heapsort on third array
		heapCount = Heap.Heapsort(Array3, heapCount);
		for (int i = 0; i < element; i++) {
			H[i] = Array3[size - i - 1];
		}
		
		//Run quicksort on fourth array
		QuickSort.quickSort(Array4, 0, size-1, c);
		quickCount = c[0];
		for (int i = 0; i < element; i++) {
			Q[i] = Array4[size - i - 1];
		}
		
		
		//Output linear search results
		System.out.println("\nLinear Search.\tKey Comparions: " + linCount);
		for (int i = 0; i < element; i++ ) {
			System.out.print(L[i] + ", ");
		}
		System.out.println();
		//Output mergesort results
		System.out.println("\nMergesort.\tKey Comparisons: " + mergeCount);
		for (int i = 0; i < element; i++) {
			System.out.print(M[i] + ", ");
		}
		System.out.println();
		//Output heapsort results
		System.out.println("\nHeapsort.\tKey Comparisons: " + heapCount);
		for (int i = 0; i < element; i++) {
			System.out.print(H[i] + ", ");
		}
		System.out.println();
		//Output quicksort results
		System.out.println("\nQuicksort.\tKey Comparisons: " + quickCount);
		for (int i = 0; i < element; i++) {
			System.out.print(Q[i] + ", ");
		}
		
		
	}

}
