package project1;

public class QuickSort {
	

    public static void quickSort(int[] A, int left, int right, int[] c) {
        
        int pivot = partition(A, left, right, c);
        if (left < right) {
            quickSort(A, left, pivot - 1, c);
            quickSort(A, pivot, right, c);
        } 
    }
    /**
     * Rearrange elements of array so that values
     * less than our pivot are on the left and 
     * values greater than our pivot are on the right
     */
    public static int partition(int[] A, int left, int right, int[] c) {
        int l = left;       //Index moving inward from the left
        int r = right;      //Index moving inward from the right
        int temp;           //Used for swapping
        int p = A[(left+right)/2];    //Pivot value comparison
        //Run until left index crosses right index
        while (l <= r) {
            while (A[l] < p) l++;   //Increase left index until a value larger than our pivot is found
            while (A[r] > p) r--;   //Decrease right index until a value smaller than our pivot is found
            //Swap elements at the left and right indices
            c[0]++;
            if (l <= r) {  
                temp = A[l];
                A[l] = A[r];
                A[r] = temp;
                //Continue index movement
                l++;
                r--;
            }
        }
        return l;
    }
/*	public static void quickSort(int[] A, int left, int right) {
		if (left < right) {
			int pivot = Partition(A, left, right);
			quickSort(A, left, pivot - 1);
			quickSort(A, pivot + 1, right);
		}
	}
	
	private static int Partition(int[] A, int left, int right) {
		int x = A[right];
		int i = left - 1;
		int temp; //temporary for swap
		for (int j = left; j < right; j++) {
			if (A[j] <= x) {
				i++;
				temp = A[j];
				A[j] = A[i];
				A[i] = temp;
				//Project1.Swap(A, i, j);
			}
		}
		Project1.Swap(A, right, i + 1);

		//Testing
		System.out.println();
		for (int c = 0; c < A.length; c++) {
			System.out.print(A[c] + ", ");
		} 
		return i + 1;
	
	}*/
	
	
}