package project1;

public class MergeSort {
	public static int mergeSort(int[] A, int left, int right, int count) {
		if (left < right) {
			int mid = (left+right)/2;
			count = mergeSort(A, left, mid, count);
			count = mergeSort(A, mid+1, right, count);
			count = merge(A, left, mid+1, right, count);
		}
		
		return count;
	}
	
	public static int merge(int[] A, int left, int mid, int right, int count) {
		 int nLeft = mid - left;
		 int nRight = right - mid + 1;
		 int[] L = new int[nLeft + 1];
		 int[] R = new int[nRight + 1];
		 
		 for (int i = 0; i < nLeft; i++) {
			 L[i] = A[left+i];
		 }
		 for (int j = 0; j < nRight; j++) {
			 R[j] = A[mid+j];
		 }

		 L[nLeft] = Integer.MAX_VALUE;
		 R[nRight] = Integer.MAX_VALUE;
		 int i = 0;
		 int j = 0;
		 for (int k = left; k <= right; k++) {
			 count++;
			 if (L[i] <= R[j]) {
				 A[k] = L[i];
				 i++;
			 }
			 else {
				 A[k] = R[j];
				 j++;
			 }
		 }
		 return count;
	}
}
