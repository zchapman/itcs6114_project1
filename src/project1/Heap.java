package project1;

public class Heap {
	
	public static int Heapsort(int A[], int count) { //Functions as delete
		int heapSize = A.length;
		Build(A);
		for (int i = heapSize - 1; i > 0; i--) {
			Project1.Swap(A, i, 0);
			heapSize--;
			count = Heapify(A, 0, heapSize, count);
		}
		return count;
	}
	
	public static void Build (int A[]) {
		int heapSize = A.length;
		for (int i = heapSize / 2 - 1; i >= 0; i --) {
			Heapify(A, i, heapSize, 0);
		}
	}
	
	public static int Heapify (int[] A, int i, int n, int count) {
		int l, r, largest;
		l = Left(i);
		r = Right(i);
		
		if ( l < n && A[l] > A[i]) {
			largest = l;
		}
		else largest = i;
		
		if ( r < n && A[r] > A[largest]) {
			largest = r;
		}
		count++;
		if (largest != i) {
			Project1.Swap(A, i, largest);
			Heapify(A, largest, n, count);
		}
		return count;
	}
	
	private static int Parent(int i) {
		if (i % 2 == 0) {
			return i/2 - 1;
		}
		return i / 2;
	}
	
	private static int Left(int i) {
		return 2 * i + 1;
	}
	
	private static int Right(int i) {
		return 2 * i + 2;
	}
}